{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# R libraries and Bioconductor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Packages and Libraries\n",
    "\n",
    "R is at heart a collection of 'packages'. There is a 'base' system that contains the truly basic commands, such as the assignment operator `->` or the command to create a vector. In addition to that, there are 'standard R' packages that are included when you install the R kernel (in the Jupyter notebook), or 'R' as a program to run either at the command line or with Rstudio. (I've shown some examples of these different ways to run R in class).\n",
    "\n",
    "### Libraries\n",
    "\n",
    "Many packages, even those included in [standard R] (https://www.r-project.org/), will need to be 'loaded' to be used. In other words, they exist on your computer (or in your container), but the R kernel doesn't know about them. This is because if it did, R would be using computer memory (RAM) to remember all their functions and variables. If all the available packages were loaded, you might not have any RAM left!\n",
    "\n",
    "A consequence of this is that you often have to tell R explicitly that you want to use a particular package. You do that using `library`. Let's read in the titanic data set to have something to play with.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic <- read.csv(\"titanic.csv\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head(titanic)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is a cool R function that will allow us to look at some random rows from a data frame. It's called `sample_n`. Let's try it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sample_n(titanic, 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Oops. It turns out `sample_n` is in the dplyr package. It's installed in your container - but R doesn't know that! Let's tell R we want to use it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "library(dplyr)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sample_n(titanic, 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Installed and installing packages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, `dplyr` is actually not part of standard R. It's *installed* separately. There are a multitude of R packages out there. Anyone can write one (yes, even you!!!). They are shared with the public using the [CRAN archive.] (https://cran.r-project.org/) In order to be listed in CRAN, packages need to meet specific criteria for documentation purposes, testing, etc.\n",
    "\n",
    "You can check to see what packages are installed using `installed.packages()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "installed.packages()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can install new packages using the command `install.packages()`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "install.packages(\"auk\", lib = \"/home/jovyan/work\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "remove.packages(\"auk\", lib = \"/home/jovyan/work\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## BioConductor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "CRAN is home to many, many R packages. But there is a whole other world out there when it comes to bioinformatics in R. It's called [BioConductor](https://bioconductor.org/). BioConductor is a comprehensive toolkit for all things having to do with high-throughput sequencing data processing and analysis. In this course, we will use the BioConductor package `DESeq2` to perform differential expression analysis. It's the end of the pipeline, after QC, clipping and trimming, aligning and counting. \n",
    "\n",
    "### Installing BioConductor packages\n",
    "\n",
    "BioConductor has it's own installation procedure (and it's own criteria for documentation, testing, etc.) - separate from CRAN. Let's have a look at the page for [DESeq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### S4 Objects\n",
    "\n",
    "R has two main types of 'objects' that can work with 'methods' (functions that are specific to those objects): S3 and S4. S3 objects are really just lists with the `class` attribute set. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat <- list(name = \"Dash\", age = 10)\n",
    "class(cat) <- \"Pet\"\n",
    "\n",
    "dog <- list(name = \"Tibbs\", age = 8)\n",
    "class(dog) <- \"Pet\"\n",
    "\n",
    "print.Pet <- function(x){\n",
    "    print(paste(\"Name:\", x$name, \"Age:\", x$age))\n",
    "}\n",
    "\n",
    "print(cat)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "S4 objects are a bit more stringent. We won't contruct our own right now, just mention that they are created with constructor functions that make sure there is consistency. \n",
    "\n",
    "Bioconductor makes extensive use of S4 objects. One of the main features of these is inheritance. This means that if an object is a 'child' of an existing object, the child inherits all the features and functions of the parent.\n",
    "\n",
    "Let's look at some of the simpler Bioconductor objects that will be used to build up more complicated structures."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "suppressPackageStartupMessages({\n",
    "  library(Biostrings)\n",
    "})\n",
    "dna <- DNAStringSet( c(\"AAACTG\", \"CCCAACCA\") ) #DNAStringSet is a constructor function for the class DNAStringSet\n",
    "\n",
    "dna\n",
    "reverseComplement(dna)  #This is a method that works on a DNAStringSet object\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This doesn't work\n",
    "\n",
    "dna2 <- c(\"AAACTG\", \"CCCAACCA\")\n",
    "reverseComplement(dna2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Where to find help:\n",
    "  \n",
    "- `class(dna)`\n",
    "- `methods(class = \"DNAStringSet\")`\n",
    "- `?\"DNAStringSet\"`, `?\"reverseComplement,DNAStringSet-method\"` (tab\n",
    "                                                                 completion!)\n",
    "- `browseVignettes(package=\"Biostrings\")`\n",
    "- https://bioconductor.org/packages\n",
    "- https://support.bioconductor.org\n",
    "\n",
    "[Biostrings]: https://bioconductor.org/packages/Biostrings\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "methods(class = \"DNAStringSet\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### GenomicRanges"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "suppressPackageStartupMessages({\n",
    "  library(GenomicRanges)\n",
    "})\n",
    "gr <- GRanges(\n",
    "  c(\"chr1:10-19\", \"chr1:15-24\", \"chr1:30-39\")\n",
    ")\n",
    "gr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Closed-interval (start and end coordinates include in range, like\n",
    "                   Ensembl; UCSC uses 1/2-open)\n",
    "- 1-based (like Ensembl; UCSC uses 0-based)\n",
    "\n",
    "Operations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(\"GRanges\")\n",
    "width(gr)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "shift(gr, 1)\n",
    "shift(gr, c(1, 2, 3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "`GRangesList`\n",
    "\n",
    "- List of `GRanges`, e.g., exons"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gene <- c(\"A\", \"A\", \"B\")\n",
    "grl <- splitAsList(gr, gene)\n",
    "grl"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Airway example\n",
    "\n",
    "We'll walk through an example using a sample data set called 'airway'. Airway is an object of type 'SummarizedExperiment'. This kind of object is the basis for many objects used in Bioconductor packages."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "library(\"airway\")\n",
    "data(\"airway\")\n",
    "se <- airway"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "str(se)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[This tutorial](https://bioconductor.org/packages/devel/bioc/vignettes/SummarizedExperiment/inst/doc/SummarizedExperiment.html) gives a great introduction to the SummarizedExperiment object. We'll take a peek, and then move on to DESeq2 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "assays(se)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "assays(se)$counts"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rowRanges(se)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colData(se)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "metadata(se)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Just a list - we can add elements\n",
    "\n",
    "metadata(se)$formula <- counts ~ dex + albut\n",
    "\n",
    "metadata(se)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# subset the first five transcripts and first three samples\n",
    "se[1:5, 1:3]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "assays(se[1:5,1:3])$counts\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "library(\"DESeq2\")\n",
    "\n",
    "\n",
    "dds <- DESeqDataSet(se, design = ~ cell + dex)\n",
    "dds\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# remove rows with less than 10 total transcripts\n",
    "\n",
    "keep <- rowSums(counts(dds)) >= 10\n",
    "dds <- dds[keep,]\n",
    "\n",
    "dds$dex\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Specify reference level\n",
    "\n",
    "dds$dex <- factor(dds$dex, levels = c(\"untrt\",\"trt\"))\n",
    "\n",
    "#alternative\n",
    "dds$dex <- relevel(dds$dex, ref = \"untrt\")\n",
    "\n",
    "dds$dex"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ddsDE <- DESeq(dds)\n",
    "res <- results(ddsDE)\n",
    "res"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
