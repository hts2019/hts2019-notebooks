.. HTS2019 documentation master file, created by
   sphinx-quickstart on Fri Jun 21 16:20:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

HTS2019 Source Materials
===================================

Source materials can bd cloned from the `HTS2019 GitLab Repository <https://gitlab.oit.duke.edu/HTS2018/HTS2019-notebooks.git>`_

.. toctree::
   :maxdepth: 2
   :caption: Unix Shell

   cliburn/R00_Review_Basics.ipynb
   cliburn/R00_Review_Basics_Scratch.ipynb
   cliburn/R01_Data_Manipulation.ipynb
   cliburn/R01_Data_Manipulation_Scratch.ipynb
   cliburn/R01_Data_Manipulation_Solutions.ipynb
   cliburn/R01_Manipulating_Data_In_R.ipynb
   cliburn/R02_Tidying_Data.ipynb
   cliburn/R02_Tidying_Data_In_R.ipynb
   cliburn/R02_Tidying_Data_Solutions.ipynb
   cliburn/R03_FileIO.ipynb
   cliburn/R04_Unsupervised_Learning.ipynb
   cliburn/R04_Unsupervised_Learning_Scratch.ipynb
   cliburn/R05_Unsupervised_Learning_More_Examples.ipynb
   cliburn/R06_Graphics_Overview.ipynb
   cliburn/R07_Graphics_Base.ipynb
   cliburn/R08_Graphics_ggplot2.ipynb
   cliburn/R09_Graphics_Exercise.ipynb
   cliburn/R09_Graphics_Exercise_Solutions.ipynb
   cliburn/Unix01_File_And_Directory.ipynb
   cliburn/Unix01_File_And_Directory_Solutions.ipynb
   cliburn/Unix02_FileIO.ipynb
   cliburn/Unix02_FileIO_Solutions.ipynb
   cliburn/Unix03_File_Storage.ipynb
   cliburn/Unix03_File_Storage_Solutions.ipynb
   cliburn/Unix04_Text_Manipulation.ipynb
   cliburn/Unix04_Text_Manipulation_Solutinos.ipynb
   cliburn/Unix05_Variables.ipynb
   cliburn/Unix05_Variables_Solutions.ipynb
   cliburn/Unix06_Bash_Bioinformatics.ipynb
   cliburn/Unix07_Capstone_Exercise.ipynb
   cliburn/Unix07_Capstone_Exercise_Solutions.ipynb
   cliburn/Unix_Appendix01_Regular_Expressions.ipynb
   cliburn/Unix_Appendix02_Reeview.ipynb

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
